import React, { useCallback, useEffect, useState } from "react";
import { interval, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import Button from "./Button";

const Main = () => {
    const [time, setTime] = useState(0);
    const [status, setStatus] = useState('');

    useEffect(() => {
        const subscribe$ = new Subject();
        interval(1000)
            .pipe(takeUntil(subscribe$))
            .subscribe(() => {
                if (status === "start") {
                    setTime(val => val + 1000);
                }
            });
        return () => {
            subscribe$.next();
            subscribe$.complete();
        };
    }, [status]);

    const start = useCallback(() => {
        setStatus("start");
    }, []);

    const stop = useCallback(() => {
        setStatus("stop");
        setTime(0);
    }, []);

    const reset = useCallback(() => {
        setTime(0);
    }, []);
    const [click, setClick] = useState(0);
    const delay = 300;

    useEffect(() => {
        setTimeout(() => {
            if (click === 1) {
                setClick(0)
            }
        }, delay);
        if (click === 2) {
            setStatus("wait")
        }
        return;
    }, [click]);

    const wait = useCallback(() => {
        setClick(prev => prev + 1);
    }, []);

    return (
        <div>
            <span> {new Date(time).toISOString().slice(11, 19)}</span>
            <div>
                {
                    status === '' || status === 'stop' || status === 'wait' ?
                        <Button onClick={start} name={'Start'} />
                        :
                        <Button onClick={stop} name={'Stop'} />
                }
                <Button onClick={wait} name={'Wait'} />
                <Button onClick={reset} name={'Reset'} />
            </div>
        </div>
    )
};

export default Main;