import React from 'react';

const Button = ({ name, onClick }) => {
    return (
        <button onClick={onClick} name={name}>{name}</button>
    )
};

export default Button;