import React, { useEffect, useState } from 'react';
import PostFilter from './components/PostFilter';
import PostForm from './components/PostForm';
import PostList from './components/PostList';
import MyButton from './components/UI/button/MyButton';
import MyModal from './components/UI/modal/MyModal';
import { usePosts } from './hooks/usePosts';
import PostService from './API/PostService';
import './styles/App.css';
import MyLoader from './components/UI/loader/MyLoader';
import { useFetching } from './hooks/useFatching';
import { getPageCount } from './utils/pages';
import Pagination from './components/UI/pagination/Pagination';

function App() {

   const [posts, setPosts] = useState([]);
   const [filter, setFilter] = useState({ sort: '', query: '' });
   const [modal, setModal] = useState(false);
   const [totalPages, setTotalPages] = useState(0);
   const [limit, setLimit] = useState(10);
   const [page, setPage] = useState(1);

   const sortedAndSearchedPosts = usePosts(posts, filter.sort, filter.query);
   const [fetchPosts, isPostsLoading, postError] = useFetching(async (limit, page) => {
      const response = await PostService.getAll(limit, page)
      setPosts(response.data)
      const totalCount = response.headers['x-total-count']
      setTotalPages(getPageCount(totalCount, limit))
   })
   const createPost = (newPost) => {
      setPosts([...posts, newPost])
      setModal(false)
   }

   useEffect(() => {
      fetchPosts(limit, page)
   }, [])

   const changePage = (page) => {
      setPage(page)
      fetchPosts(limit, page)
   }

   const removePost = (post) => {
      setPosts(posts.filter(p => p.id !== post.id))
   }

   return (
      <div className="App">
         <MyButton onClick={() => setModal(true)}>Add post</MyButton>
         <MyModal visible={modal} setVisible={setModal}>
            <PostForm create={createPost} />
         </MyModal>
         <hr style={{ margin: '15px 0px' }} />
         <PostFilter
            filter={filter}
            setFilter={setFilter}
         />
         {postError &&
            <h1>Error: ${postError}</h1>
         }
         {isPostsLoading
            ? <MyLoader />
            : <PostList remove={removePost} posts={sortedAndSearchedPosts} title={'Post List'} />
         }
         <Pagination
            page={page}
            changePage={changePage}
            totalPages={totalPages}
         />
      </div>
   );
}

export default App;
