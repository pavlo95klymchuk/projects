import { createSlice } from "@reduxjs/toolkit";

const initialState = JSON.parse(localStorage.getItem('cart')) || [];

const cartSlice = createSlice({
   name: "cart",
   initialState,
   reducers: {
      addToCart: (state, action) => {
         const prevCartStore = JSON.parse(localStorage.getItem('cart'));
         prevCartStore ? localStorage.setItem('cart', JSON.stringify([...prevCartStore, action.payload])) : localStorage.setItem('cart', JSON.stringify([action.payload]))
         state.push(action.payload);

      },
      removeFromCart: (state, action) => {
         state = state.filter((item) => item.id !== action.payload);
      },
   },
});

export const { addToCart, removeFromCart } = cartSlice.actions;

export default cartSlice.reducer;
