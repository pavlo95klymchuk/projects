import { combineReducers } from "redux";
import cartReducer from "./cartSlice.js";
import favoriteSlice from "./favoriteSlice.js";

const rootReducer = combineReducers({
   cart: cartReducer,
   favorite: favoriteSlice,
});

export default rootReducer;