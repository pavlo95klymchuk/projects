import { createSlice } from "@reduxjs/toolkit";

const initialState = JSON.parse(localStorage.getItem('favorite')) || [];

const favoriteSlice = createSlice({
   name: "favorite",
   initialState,
   reducers: {
      addToFavorite: (state, action) => {
         const prevCartStore = JSON.parse(localStorage.getItem('favorite'));
         let newFavorite = []
         if (prevCartStore && prevCartStore.length > 0) {
            console.log(prevCartStore);
            newFavorite = prevCartStore.filter(el => el.id !== action.payload.id)
            console.log();
            localStorage.setItem('favorite', JSON.stringify(newFavorite))
            return state.filter(el => el.id !== action.payload.id);
         } else {
            localStorage.setItem('favorite', JSON.stringify([action.payload]))
            state.push(action.payload);
         }
         // localStorage.setItem('favorite', JSON.stringify(newLiked));
         // prevCartStore ? localStorage.setItem('favorite', JSON.stringify([...prevCartStore, action.payload])) : localStorage.setItem('favorite', JSON.stringify([action.payload]))
         // state.push(action.payload);

      },
      removeFromFavorite: (state, action) => {
         state = state.filter((item) => item.id !== action.payload);
      },
   },
});

export const { addToFavorite, removeFromFavorite } = favoriteSlice.actions;

export default favoriteSlice.reducer;
