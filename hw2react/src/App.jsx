import React, { useState, useEffect } from "react";
import CardList from "./components/CardList/CardList.jsx";
import Header from "./components/Header/Header.jsx";
import Modal from "./components/Modal/Modal.jsx";
import Button from "./components/Button/Button.jsx";
import { useSelector, useDispatch } from "react-redux";
import { addToCart, removeFromCart } from "./store/cartSlice.js";
import { addToFavorite } from "./store/favoriteSlice.js";
import './App.css';

function App() {
   const [isModalActive, setModalActive] = useState(false);
   const [productModal, setProductModal] = useState({});


   const cart = useSelector(state => state.cart);
   const favorite = useSelector(state => state.favorite)

   const dispatch = useDispatch();

   const activateModal = (product) => {
      setModalActive(true);
      setProductModal(product);
   }

   const deActivateModal = () => {
      setModalActive(false);
   }

   const addProductToCart = (product) => {
      dispatch(addToCart(product));
      deActivateModal();
   }

   const toggleLiked = (product) => {
      dispatch(addToFavorite(product));
   }

   useEffect(() => {
      // Виконується один раз при завантаженні компонента
      // Можна використовувати для ініціалізації даних або налаштувань
   }, []);

   return (
      <>
         <Header cart={cart} liked={favorite} />
         <CardList
            activateModal={activateModal}
            toggleLiked={toggleLiked}
            liked={favorite}
         />
         {isModalActive && (
            <Modal
               header={`Buy product ${productModal.code}?`}
               text={`Do you really want to purchase ${productModal.name}?`}
               onModalClose={deActivateModal}
               actions={
                  <div className="modalButtons">
                     <Button
                        text="Add"
                        onClick={() => addProductToCart(productModal)}
                     />
                     <Button text="Cancel" onClick={deActivateModal} />
                  </div>
               }
            />
         )}
      </>
   );
}

export default App;