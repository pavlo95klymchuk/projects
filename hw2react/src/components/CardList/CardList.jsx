import './style.css';
import Card from "../Card/Card.jsx";
import React, { Component } from "react";

class Products extends Component {

   constructor(props) {
      super(props);
      this.state = { products: [] }
   }

   componentDidMount() {

      fetch('cards.json')
         .then(response => response.json())
         .then(data => this.setState({ products: data }))

   }


   render() {

      const products = this.state.products

      return (
         <div className='productsList'>
            {products.map(el => <Card
               product={el}
               key={el.id}
               activateModal={this.props.activateModal}
               toggleLiked={this.props.toggleLiked}
               liked={this.props.liked.map(liked => liked.id).includes(el.id)}
            />)}
         </div>
      );
   }
}

export default Products;
