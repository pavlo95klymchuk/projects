import React, { useState } from "react";
import Button from "../Button/Button.jsx";
import { faHeart, faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ProductCard = (props) => {
   const { product, liked, activateModal, toggleLiked } = props;
   const { id, name, price, imageUrl, color } = product;
   const [isProductLiked, setIsProductLiked] = useState(liked || false);

   return (
      <div className="card" key={id} id={id}>
         <div
            className="cardImage"
            style={{
               backgroundImage: `url(${imageUrl})`,
            }}
         >
            <FontAwesomeIcon
               icon={faStar}
               onClick={() => {
                  toggleLiked(product);
                  setIsProductLiked((prevState) => !prevState);
               }}
               style={{
                  color: `${isProductLiked ? "red" : "#1165B2"}`,
               }}
            />
         </div>

         <h3 >{name}</h3>

         <div className="cardColor">
            <p>Available color: </p>
            <div className="cardColor" style={{ backgroundColor: `${color}` }} />
         </div>

         <p className="cardPrice">
            $ {price}
         </p>

         <Button
            text="Add to cart"
            onClick={() => {
               activateModal(product);
            }}
         />
      </div>
   );
};

export default ProductCard;






